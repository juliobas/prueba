import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

declare global {
  interface Window {
    mySwipeS: any;
    mySwipeI: any;
  }
}
window.mySwipeS = window.mySwipeS || {};
window.mySwipeI = window.mySwipeI || {};

platformBrowserDynamic().bootstrapModule(AppModule);
