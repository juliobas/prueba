import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-register-cmp',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']

})

export class RegisterComponent implements OnInit {
    test: Date = new Date();
    public color:string;

    constructor(){
    	this.color = "#1EB7DE";
    }

    ngOnInit() {
    }

    signOut() {
   window.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:4200/pages/login";

   console.log('User signed out.');
  }

}
