import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ListModulosComponent } from './list-modulos/list-modulos.component';
import { ListModulosComponentRoutes } from './configuracion.routing';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ListModulosComponentRoutes)
  ],
  declarations: [ListModulosComponent]
})
export class ConfiguracionModule { }
