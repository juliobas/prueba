import { Component, OnInit } from '@angular/core';

export interface Modulos{
  name: string;
  description: string;
  icon: string;
}

export const modulos: Modulos[] = [
  {
    name: 'Usuarios',
    description: 'Roles, Permisos, Percentajes, Activar / Desbloquear',
    icon: 'assets/img/Iconos/usuarios.svg'
  },
  {
    name: 'Tratamientos',
    description: 'Tratamientos y Vinculaciones, Categorias Clinicas',
    icon: 'assets/img/Iconos/tratamientos.svg'
  },
  {
    name: 'Documentos',
    description: 'Formatos, Plantillas, e Imprimibles en General',
    icon: 'assets/img/Iconos/documentos.svg'
  },
  {
    name: 'Empresa',
    description: 'Datos, Ubicación, Teléfonos y Logo',
    icon: 'assets/img/Iconos/empresa.svg'
  },
  {
    name: 'Sucursales',
    description: 'Datos, Ubicación, Teléfonos, etc',
    icon: 'assets/img/Iconos/sucursales.svg'
  },
  {
    name: 'Calendario',
    description: 'Intervalos, Inicio de Semana, Feriados, Entrada y Salida',
    icon: 'assets/img/Iconos/calendario.svg'
  },
  {
    name: 'Egresos',
    description: 'Fijos, Egresos, Nómina',
    icon: 'assets/img/Iconos/egresos.svg'
  },
  {
    name: 'Inventario',
    description: 'Insumos, Productos, Descartables',
    icon: 'assets/img/Iconos/inventario.svg'
  },
  {
    name: 'Factor Electrónico',
    description: 'Configuración de Token, N° de Facturas, Tax, etc',
    icon: 'assets/img/Iconos/factor-electronico.svg'
  },
  {
    name: 'Convenios',
    description: 'Añadir, Relacionar con Tratamiento, Agregar % Descuento',
    icon: 'assets/img/Iconos/convenios.svg'
  },
  {
    name: 'Domínio',
    description: 'Web, Email Corporativo, Subdominio',
    icon: 'assets/img/Iconos/dominios.svg'
  },
  {
    name: 'Apariencia',
    description: 'Colores, Iconos, Simbolos',
    icon: 'assets/img/Iconos/apariencia.svg'
  },
  {
    name: 'Almacenamiento',
    description: 'Estado, Cuotas, Liberar Espacio',
    icon: 'assets/img/Iconos/almacenamiento.svg'
  },
  {
    name: 'Privacidad',
    description: 'Lorem Ipsum',
    icon: 'assets/img/Iconos/privacidad.svg'
  },
  {
    name: 'Contactos',
    description: 'Lorem Ipsum',
    icon: 'assets/img/Iconos/contactos.svg'
  },
  {
    name: 'Cuenta',
    description: 'Perfiles de Usuario, Datos, Suscripciones',
    icon: 'assets/img/Iconos/cuenta.svg'
  },
  
]

@Component({
  selector: 'app-list-modulos',
  templateUrl: './list-modulos.component.html',
  styleUrls: ['./list-modulos.component.css']
})
export class ListModulosComponent implements OnInit {
  
  public modulos: any[];

  constructor() { }

  ngOnInit() {
    this.modulos = modulos;
  }

}
