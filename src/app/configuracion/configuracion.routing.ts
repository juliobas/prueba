import { Routes } from '@angular/router';

import { ListModulosComponent } from './list-modulos/list-modulos.component';

export const ListModulosComponentRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: ListModulosComponent
    }]
}
];
