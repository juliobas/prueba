import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';
import { MaterialModule } from '../app.module';

import { OdontogramaComponent } from './odontograma.component';
import { OdontogramaRoutes } from './odontograma.routing';
import {
  OdontogramaBocaComponent,
  ModalUdComponent
} from './odontograma-boca/odontograma-boca.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(OdontogramaRoutes),
    FormsModule,
    MdModule,
    MaterialModule
  ],
  declarations: [
    OdontogramaComponent,
    OdontogramaBocaComponent,
    ModalUdComponent
  ],
  entryComponents: [ModalUdComponent]
})
export class OdontogramaModule {}
