import {
  Component,
  OnInit,
  Renderer2,
  ElementRef,
  HostListener
} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import swal from 'sweetalert2';

@Component({
  selector: 'app-odontograma',
  templateUrl: './odontograma.component.html',
  styleUrls: ['./odontograma.component.css']
})
export class OdontogramaComponent implements OnInit {
  public state;
  constructor(private renderer: Renderer2, private el: ElementRef) {}

  @HostListener('window:resize')
  onResize() {
    this.appOrientation();
    this.alertScreen();
  }

  ngOnInit() {
    const ps1 = new PerfectScrollbar('.historial', {
      wheelSpeed: 1,
      maxScrollbarLength: 70,
      suppressScrollX: true
    });

    this.appOrientation();
    this.alertScreen();
  }

  public alertScreen() {
    if (this.state) {
      swal({
        title: '¡Debes tener el celular o tablet en horizontal!',
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-success'
      }).catch(swal.noop);
    }
  }

  public appOrientation() {
    if (
      (window.screen.height < 601 && window.screen.width < 1000) ||
      (window.screen.height < 1000 && window.screen.width < 601)
    ) {
      switch (screen['orientation'].angle) {
        case -90:
        case 90:
          this.renderer.removeAttribute(
            document.getElementById('permanentes').children[0],
            'style'
          );
          this.renderer.removeAttribute(
            document.getElementById('temporales').children[0],
            'style'
          );
          this.renderer.removeAttribute(
            document.getElementById('Indicadores').children[0],
            'style'
          );
          this.state = false;
          break;
        default:
          this.renderer.setAttribute(
            document.getElementById('permanentes').children[0],
            'style',
            'display: none'
          );
          this.renderer.setAttribute(
            document.getElementById('temporales').children[0],
            'style',
            'display: none'
          );
          this.renderer.setAttribute(
            document.getElementById('Indicadores').children[0],
            'style',
            'display: none'
          );
          this.state = true;
          break;
      }
    } else if (window.innerWidth >= 1200) {
      this.renderer.removeAttribute(
        document.getElementById('permanentes').children[0],
        'style'
      );
      this.renderer.removeAttribute(
        document.getElementById('temporales').children[0],
        'style'
      );
      this.renderer.removeAttribute(
        document.getElementById('Indicadores').children[0],
        'style'
      );
      this.state = false;
    }
  }
}
