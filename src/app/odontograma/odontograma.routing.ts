import { Routes } from '@angular/router';

import { OdontogramaComponent } from './odontograma.component';

export const OdontogramaRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: '',
        component: OdontogramaComponent
    }]
}
];
