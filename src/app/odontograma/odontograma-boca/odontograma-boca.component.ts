import {
  Component,
  Renderer2,
  ElementRef,
  OnInit,
  AfterViewChecked,
  AfterViewInit,
  Inject,
  HostListener,
  ViewChild,
  Input
} from '@angular/core';
import { FormControl } from '@angular/forms';
import PerfectScrollbar from 'perfect-scrollbar';
import Swiper from 'swiper/dist/js/swiper.js';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatButtonToggleGroup
} from '@angular/material';

export interface PiezasInfo {
  name: string;
  src: string;
}

export interface CondInfo {
  value: string;
  viewValue: string;
}

export const piezasS: PiezasInfo[] = [
  {
    name: '18',
    src: 'assets/img/odontograma/piezas/18.svg'
  },
  {
    name: '17',
    src: 'assets/img/odontograma/piezas/17.svg'
  },
  {
    name: '16',
    src: 'assets/img/odontograma/piezas/16.svg'
  },
  {
    name: '15',
    src: 'assets/img/odontograma/piezas/15.svg'
  },
  {
    name: '14',
    src: 'assets/img/odontograma/piezas/14.svg'
  },
  {
    name: '13',
    src: 'assets/img/odontograma/piezas/13.svg'
  },
  {
    name: '12',
    src: 'assets/img/odontograma/piezas/12.svg'
  },
  {
    name: '11',
    src: 'assets/img/odontograma/piezas/11.svg'
  },
  {
    name: '21',
    src: 'assets/img/odontograma/piezas/21.svg'
  },
  {
    name: '22',
    src: 'assets/img/odontograma/piezas/22.svg'
  },
  {
    name: '23',
    src: 'assets/img/odontograma/piezas/23.svg'
  },
  {
    name: '24',
    src: 'assets/img/odontograma/piezas/24.svg'
  },
  {
    name: '25',
    src: 'assets/img/odontograma/piezas/25.svg'
  },
  {
    name: '26',
    src: 'assets/img/odontograma/piezas/26.svg'
  },
  {
    name: '27',
    src: 'assets/img/odontograma/piezas/27.svg'
  },
  {
    name: '28',
    src: 'assets/img/odontograma/piezas/28.svg'
  }
];

export const piezasI: PiezasInfo[] = [
  {
    name: '48',
    src: 'assets/img/odontograma/piezas/48.svg'
  },
  {
    name: '47',
    src: 'assets/img/odontograma/piezas/47.svg'
  },
  {
    name: '46',
    src: 'assets/img/odontograma/piezas/46.svg'
  },
  {
    name: '45',
    src: 'assets/img/odontograma/piezas/45.svg'
  },
  {
    name: '44',
    src: 'assets/img/odontograma/piezas/44.svg'
  },
  {
    name: '43',
    src: 'assets/img/odontograma/piezas/43.svg'
  },
  {
    name: '42',
    src: 'assets/img/odontograma/piezas/42.svg'
  },
  {
    name: '41',
    src: 'assets/img/odontograma/piezas/41.svg'
  },
  {
    name: '31',
    src: 'assets/img/odontograma/piezas/31.svg'
  },
  {
    name: '32',
    src: 'assets/img/odontograma/piezas/32.svg'
  },
  {
    name: '33',
    src: 'assets/img/odontograma/piezas/33.svg'
  },
  {
    name: '34',
    src: 'assets/img/odontograma/piezas/34.svg'
  },
  {
    name: '35',
    src: 'assets/img/odontograma/piezas/35.svg'
  },
  {
    name: '36',
    src: 'assets/img/odontograma/piezas/36.svg'
  },
  {
    name: '37',
    src: 'assets/img/odontograma/piezas/37.svg'
  },
  {
    name: '38',
    src: 'assets/img/odontograma/piezas/38.svg'
  }
];

export const cond: CondInfo[] = [
  {
    value: 'caries',
    viewValue: 'Caries ejemplo'
  }
];

export interface Coords {
  x?: number;
  y?: number;
}

@Component({
  selector: 'app-odontograma-boca',
  templateUrl: './odontograma-boca.component.html',
  styleUrls: ['./odontograma-boca.component.css']
})
export class OdontogramaBocaComponent
  implements OnInit, AfterViewChecked, AfterViewInit {
  @Input() tipo: string;
  public itemsS: any[];
  public itemsI: any[];
  public conds: any[];
  public swipers: any[] = [];
  public selectState: any[2][16] = [
    [
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true)
    ],
    [
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true),
      new FormControl(true)
    ]
  ];

  public selectedValue: any[2][16] = [
    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
    ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
  ];
  public slideIndex: any[][];
  touchStartCoords: Coords = { x: -1, y: -1 }; // X and Y coordinates on mousedown or touchstart events.
  touchEndCoords: Coords = { x: -1, y: -1 }; // X and Y coordinates on mouseup or touchend events.
  direction = 'undefined'; // Swipe direction
  startTime = 0; // Time on swipeStart
  elapsedTime = 0; // Elapsed time between swipeStart and swipeEnd
  targetElement; // Element to delegate
  eventElement;

  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    // window.mySwipeS = new Swipe(document.getElementById('sliderS'));
    // window.mySwipeI = new Swipe(document.getElementById('sliderI'));
    // const mySwiperS = new Swiper('#swiperS');
    // const mySwiperI = new Swiper('#swiperI');
    this.itemsS = piezasS.filter(items => items);
    this.itemsI = piezasI.filter(items => items);
    this.conds = cond.filter(items => items);
    this.slideIndex = [
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1],
      [1, 1, 1, 1]
    ];
  }

  ngAfterViewInit() {
    // viewChild is set after the view has been initialized
    // const swiper = new Swiper('.swiper-container');
    for (let i = 0; i < this.itemsS.length; i++) {
      this.showSlides(
        this.slideIndex[i][0],
        'S_s_' + this.tipo + '_' + this.itemsS[i].name,
        0,
        i
      );
      this.showSlides(
        this.slideIndex[i][1],
        'S_i_' + this.tipo + '_' + this.itemsS[i].name,
        1,
        i
      );
      this.showSlides(
        this.slideIndex[i][2],
        'I_s_' + this.tipo + '_' + this.itemsI[i].name,
        2,
        i
      );
      this.showSlides(
        this.slideIndex[i][3],
        'I_i_' + this.tipo + '_' + this.itemsI[i].name,
        3,
        i
      );
    }

    this.targetElement = document.querySelectorAll('.swipe-slide');
    [].forEach.call(this.targetElement, item => {
      this.addMultipleListeners(item, 'mousedown touchstart', this.swipeStart);
      this.addMultipleListeners(item, 'mousemove touchmove', this.swipeMove);
      this.addMultipleListeners(item, 'mouseup touchend', this.swipeEnd);
    });
  }

  ngAfterViewChecked() {
    // viewChild is updated after the view has been checked
  }

  public setState(event) {
    let target;
    if ('srcElement' in event) {
      target = event.srcElement;
    } else {
      target = event.target;
    }
    const state = target.getAttribute('data-state');
    if (state === '0') {
      target.setAttribute('style', 'background: #f00');
      target.setAttribute('data-state', '1');
    }
    if (state === '1') {
      target.setAttribute('style', 'background: #00f');
      target.setAttribute('data-state', '2');
    }
    if (state === '2') {
      target.removeAttribute('style');
      target.setAttribute('data-state', '0');
    }

    const cSiblings = target.parentElement.children;

    const ud: number = parseInt(
      target.parentElement.parentElement.getAttribute('data-ud'),
      10
    );

    const id: string = target.parentElement.parentElement.id;

    for (let i = 0, falses = 0; i < cSiblings.length; i++) {
      if (cSiblings[i].getAttribute('data-state') === '1') {
        if (
          target.parentElement.parentElement.getAttribute('data-pos') === 'S'
        ) {
          this.selectState[0][ud].setValue(false);
        } else {
          this.selectState[1][ud].setValue(false);
        }
        break;
      }

      if (falses === cSiblings.length - 1) {
        if (
          target.parentElement.parentElement.getAttribute('data-pos') === 'S'
        ) {
          this.selectState[0][ud].setValue(true);
          this.selectedValue[0][ud] = '';
        } else {
          this.selectState[1][ud].setValue(true);
          this.selectedValue[1][ud] = '';
        }
      }
      falses += 1;
    }
  }

  public setPosUD(event, parent: boolean = false, deep: number = null) {
    let id: string;
    let state;
    let target;
    if ('srcElement' in event) {
      target = event.srcElement;
    } else {
      target = event.target;
    }
    let elem;
    if (parent !== false) {
      if (deep === 1) {
        id = target.parentElement.id;
        state = target.parentElement.getAttribute('data-state');
        elem = target.parentElement;
      } else if (deep === 2) {
        id = target.parentElement.parentElement.id;
        state = target.parentElement.parentElement.getAttribute('data-state');
        elem = target.parentElement.parentElement;
      }
    } else {
      id = target.id;
      state = target.getAttribute('data-state');
      elem = target;
    }
    if (state === '0') {
      this.renderer.addClass(document.getElementById(id + '_ud'), 'pos1');
      this.renderer.addClass(document.getElementById(id + '_l'), 'pos1');
      elem.setAttribute('data-state', '1');
    }
    if (state === '1') {
      this.renderer.removeClass(document.getElementById(id + '_ud'), 'pos1');
      this.renderer.removeClass(document.getElementById(id + '_l'), 'pos1');
      this.renderer.addClass(document.getElementById(id + '_ud'), 'pos2');
      this.renderer.addClass(document.getElementById(id + '_l'), 'pos2');
      elem.setAttribute('data-state', '2');
    }
    if (state === '2') {
      this.renderer.removeClass(document.getElementById(id + '_ud'), 'pos1');
      this.renderer.removeClass(document.getElementById(id + '_l'), 'pos1');
      this.renderer.removeClass(document.getElementById(id + '_ud'), 'pos2');
      this.renderer.removeClass(document.getElementById(id + '_l'), 'pos2');
      elem.setAttribute('data-state', '0');
    }
  }

  // Next/previous controls
  public plusSlides(n, id, i, ni) {
    this.showSlides((this.slideIndex[ni][i] += n), id, i, ni);
  }

  // Thumbnail image controls
  public currentSlide(n, id, i, ni) {
    this.showSlides((this.slideIndex[ni][i] = n), id, i, ni);
  }

  public showSlides(n, id, ix, ni) {
    let i;
    const slides = document.getElementsByClassName(id);
    if (n > slides.length) {
      this.slideIndex[ni][ix] = 1;
    }
    if (n < 1) {
      this.slideIndex[ni][ix] = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].setAttribute('style', 'display: none');
    }
    slides[this.slideIndex[ni][ix] - 1].setAttribute('style', 'display: block');
  }

  public swipeStart(e) {
    e = e ? e : window.event;
    e = 'changedTouches' in e ? e.changedTouches[0] : e;
    this.touchStartCoords = { x: e.pageX, y: e.pageY };
    this.startTime = new Date().getTime();
    if ('target' in e) {
      this.eventElement = e.target;
    } else {
      this.eventElement = e.srcElement;
    }
  }

  public swipeMove(e) {
    e = e ? e : window.event;
    e.preventDefault();
  }

  public swipeEnd(e) {
    e = e ? e : window.event;
    e = 'changedTouches' in e ? e.changedTouches[0] : e;
    this.touchEndCoords = {
      x: e.pageX - this.touchStartCoords.x,
      y: e.pageY - this.touchStartCoords.y
    };
    this.elapsedTime = new Date().getTime() - this.startTime;
    if (this.elapsedTime <= 1000) {
      const xmax: boolean = Math.abs(this.touchEndCoords.x) >= 2;
      const ymax: boolean = Math.abs(this.touchEndCoords.x) >= 2;
      if (xmax && ymax) {
        this.direction = this.touchEndCoords.x < 0 ? 'left' : 'right';
        if (this.eventElement !== undefined) {
          if (this.eventElement.tagName === 'IMG') {
            const classess = this.eventElement.parentElement
              .getAttribute('class')
              .split(' ');
          } else {
            const classess = this.eventElement.getAttribute('class').split(' ');
          }
          const classess = this.eventElement.getAttribute('class').split(' ');
          const event = new MouseEvent('click', { bubbles: true });
          const els = this.eventElement.parentElement.children;
          switch (this.direction) {
            case 'left':
              for (let i = 0; i < els.length; i++) {
                if (els[i].getAttribute('class') === 'next-slide') {
                  els[i].dispatchEvent(event);
                  break;
                }
              }
              break;
            case 'right':
              for (let i = 0; i < els.length; i++) {
                if (els[i].getAttribute('class') === 'prev-slide') {
                  els[i].dispatchEvent(event);
                  break;
                }
              }
              break;
          }
        }
      }
    }
  }

  public addMultipleListeners(el, s, fn) {
    const evts = s.split(' ');
    for (let i = 0; i < evts.length; i++) {
      el.addEventListener(evts[i], fn, false);
    }
  }

  openDialog(e, iNum) {
    const eTarget = 'srcElement' in e ? e.srcElement : e.target;
    const udNum = iNum;
    const screenHeight = window.screen.height < 798 ? '95vh' : '80vh';
    const screenWidth = window.screen.width < 961 ? '90vw' : '80vw';
    this.dialog.open(ModalUdComponent, {
      width: screenWidth,
      height: screenHeight,
      data: {
        elem: eTarget,
        itemS: this.itemsS,
        itemI: this.itemsI,
        udNum: iNum
      }
    });
  }
}

@Component({
  selector: 'app-modal-ud',
  templateUrl: './modal-ud.component.html',
  styleUrls: ['./modal-ud.component.css']
})
export class ModalUdComponent implements OnInit {
  checked = false;
  panelOpenState = false;
  psModal: PerfectScrollbar;

  @ViewChild('groupRecesion') recesionGroup: MatButtonToggleGroup;
  @ViewChild('groupMovilidad') movilidadGroup: MatButtonToggleGroup;
  @ViewChild('groupImplante') implanteGroup: MatButtonToggleGroup;
  @ViewChild('groupEndodoncia') endodonciaGroup: MatButtonToggleGroup;
  @ViewChild('groupCorona') coronaGroup: MatButtonToggleGroup;
  @ViewChild('groupSellante') sellanteGroup: MatButtonToggleGroup;
  @ViewChild('groupDiastema') diastemaGroup: MatButtonToggleGroup;
  @ViewChild('groupSupernumerario') supernumerarioGroup: MatButtonToggleGroup;
  constructor(
    public dialogRef: MatDialogRef<ModalUdComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.psModal = new PerfectScrollbar('.mat-dialog-content', {
      wheelSpeed: 1,
      maxScrollbarLength: 70,
      suppressScrollX: true
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  resetGroup(group: string) {
    if (group === 'recesion') {
      this.recesionGroup.selected = null;
    }
    if (group === 'movilidad') {
      this.movilidadGroup.selected = null;
    }
    if (group === 'implante') {
      this.implanteGroup.selected = null;
    }
    if (group === 'endodoncia') {
      this.endodonciaGroup.selected = null;
    }
    if (group === 'corona') {
      this.coronaGroup.selected = null;
    }
    if (group === 'sellante') {
      this.sellanteGroup.selected = null;
    }
    if (group === 'diastema') {
      this.diastemaGroup.selected = null;
    }
    if (group === 'supernumerario') {
      this.supernumerarioGroup.selected = null;
    }
  }

  @HostListener('window:resize')
  onResize() {
    this.psModal.update();
  }
}
