import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OdontogramaBocaComponent } from './odontograma-boca.component';

describe('OdontogramaBocaComponent', () => {
  let component: OdontogramaBocaComponent;
  let fixture: ComponentFixture<OdontogramaBocaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OdontogramaBocaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OdontogramaBocaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
