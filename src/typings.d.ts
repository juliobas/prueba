/* SystemJS module definition */
// declare var module: {
//   id: string;
// };

export declare const SELECT_PANEL_MAX_HEIGHT = 200;
export declare const SELECT_PANEL_PADDING_X = 10;
export declare const SELECT_ITEM_HEIGHT_EM = 1.5;

interface SwipeOptions {
  startSlide?: number;
  speed?: number;
  auto?: number;
  draggable?: boolean;
  continuous?: boolean;
  autoRestart?: boolean;
  disableScroll?: boolean;
  stopPropagation?: boolean;
  callback?: (index: number, elem: HTMLElement, dir: number) => void;
  transitionEnd?: (index: number, elem: HTMLElement) => void;
}

declare class Swipe {
  constructor(container: HTMLElement, options?: SwipeOptions);
  prev(): void;
  next(): void;
  getPos(): number;
  getNumSlides(): number;
  slide(index: number, duration: number): void;
  restart(): void;
  stop(): void;
  setup(options?: SwipeOptions): void;
  kill(): void;
  disable(): void;
  enable(): void;
}
